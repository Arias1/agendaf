package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
            DefinirTabla.Contacto._ID, DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1, DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2, DefinirTabla.Contacto.COLUMN_NAME_DIRECCION, DefinirTabla.Contacto.COLUMN_NAME_NOTAS, DefinirTabla.Contacto.COLUMN_NAME_FAVORITE
    };

    public AgendaContacto(Context context){
        this.context = context;
        this.agendaDbHelper = new AgendaDbHelper(this.context);
    }

    public void openDatabase() {
        db = agendaDbHelper.getWritableDatabase();
    }

    public long insertarContacto(Contacto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_FAVORITE, c.getFavorite());

        return db.insert(DefinirTabla.Contacto.TABLE_NAME, null, values);
    }

    public long actualizarContacto(Contacto c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.COLUMN_NAME_FAVORITE, c.getFavorite());

        String criterio = DefinirTabla.Contacto._ID + " = " + id;

        return db.update(DefinirTabla.Contacto.TABLE_NAME, values, criterio, null);
    }

    public long eliminarContacto(long id){
        String criterio = DefinirTabla.Contacto._ID + " = " + id;
        return db.delete(DefinirTabla.Contacto.TABLE_NAME, criterio, null);
    }

    public Contacto leerContacto(Cursor cursor){
        Contacto c = new Contacto();

        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDireccion(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorite(cursor.getInt(6));

        return c;
    }

    public Contacto getContacto(long id){
        Contacto contacto = null;
        SQLiteDatabase db = this.agendaDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME, columnToRead, DefinirTabla.Contacto._ID + " = ?", new String[] {String.valueOf(id)}, null, null, null);
        if (c.moveToFirst()){
            contacto = leerContacto(c);
        }
        c.close();
        return contacto;
    }

    public ArrayList<Contacto> allContactos(){
        ArrayList<Contacto> contactos = new ArrayList<>();
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contacto c = leerContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void cerrar(){
        agendaDbHelper.close();
    }

}
